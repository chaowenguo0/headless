import {chromium} from 'playwright-chromium'

let browser = await chromium.launch({channel:'chrome', args:['--disable-blink-features=AutomationControlled', '--start-maximized'], ignoreDefaultArgs:['--enable-automation'], headless:false})
let context = await browser.newContext({recordVideo:{dir:'videos'}, viewport:null})
let page = await context.newPage()
await page.goto('https://bot.sannysoft.com')
globalThis.console.log(await page.evaluate(() => [globalThis.navigator.userAgentData.brands, globalThis.navigator.webdriver]))
await page.screenshot({path:'chrome.jpg', fullPage: true})
await page.waitForTimeout(1000 * 60)
await browser.close()

/*import {firefox} from 'playwright-firefox'

const browser = await firefox.launch({executablePath:'/usr/bin/firefox'})
const context = await browser.newContext()
const page = await context.newPage()
await page.goto('https://bot.sannysoft.com')
globalThis.console.log(await page.evaluate(() => globalThis.navigator.webdriver))
await page.screenshot({path:'firefox.png', fullPage: true})
await browser.close()*/
